@extends('admin.widget.index')
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="page-title">
                <div class="title_left">
                    <a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a><span>/</span><a href="admin/student" style="font-size: 20px;">Nhân viên</a>
                    <h3>Danh sách nhân viên</h3>
            </div>
        </div>
            <div class="text-center">
                
                @if(session('success'))
                <div class="alert-success text-center" style="width: 100%;display: block;padding: 10px;margin: 20px 0px;">
                    {{session('success')}}
                </div>
            @endif 
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                        <table class="table table-hover table-bordered table12" style="margin-top: 15px;">
                        <thead>
                            <tr>
                                <th style="width: 5%; text-align: center;">ID</th>
                                <th style="width: 15%;">Tên</th>
                                <th style="width: 15%;">Phòng Ban</th>
                                <th style="width: 25%;">Ngày sinh</th>
                                <th style="width: 20%;">Địa chỉ</th>
                                <th style="width: 20%;">Giới tính</th>
                                <th style="width: 20%;">Lương</th>
                                <th style="width: 20%;">Chức vụ</th>
                                <th style="width: 20%;">Ngày bắt đầu</th>
                                <th style="width: 20%;">Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody >
                            @foreach($emp as $item)
                                <tr>
                                    <td class="text-center"><span>{{$item->id}}</span></td>
                                    <td class="text-center"><span>{{$item->name}}</span></td>
                                    <td><span>{{$item->department_id}}</span></td>
                                    <td><span>{{$item->date_of_birth}}</span></td>
                                    <td><span>{{$item->address}}</span></td>
                                    <td><span>{{$item->gender}}</td>
                                    <td><span>{{$item->salary}}</td>
                                    <td><span>{{$item->position}}</td>
                                    <td><span>{{$item->date_start}}</td>
                                    <td style="width: 15%">
                                        @if(($item->status) == 1)
                                        <a onclick="return confirm('Bạn có muốn thay đổi trạng thái nhân viên không?')" href="admin/employees/off/{{$item->id}}" class="btn btn-sm btn-danger" title="Khoá">Tắt</a>
                                        @else
                                        <a onclick="return confirm('Bạn có muốn thay đổi trạng thái nhân viên không?')" href="admin/employees/on/{{$item->id}}" class="btn btn-sm btn-success" title="Khoá">Bật</a>
                                        @endif
                                        <a class="btn btn-sm btn-warning" href='javascript:window.location.href ="admin/employees/edit/{{$item->id}}"' title="">Sửa</a>
                                        <a onclick="return confirm('Bạn có muốn xoá')" class="btn btn-sm btn-danger" href="javascript:window.location.href ='admin/employees/destroy/{{$item->id}}'" title="">Xoá</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
