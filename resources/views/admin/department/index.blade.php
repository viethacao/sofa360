@extends('admin.widget.index')
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="page-title">
                <div class="title_left">
                    <a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a><span>/</span><a href="admin/department" style="font-size: 20px;">Phòng ban</a>
                    <h3>Danh sách phòng ban</h3>
            </div>
        </div>
            <div class="text-center">
               
                @if(session('success'))
                    <div class="alert-success text-center" style="width: 100%;display: block;padding: 10px;margin: 20px 0px;">
                        {{session('success')}}
                    </div>
                @endif 
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                        <table class="table table-hover table-bordered table12" style="margin-top: 15px;">
                        <thead>
                            <tr>
                                <th style="width: 5%; text-align: center;">ID</th>
                                <th style="width: 15%; text-align: center;">Tên phòng ban</th>
                                <th style="width: 20%; text-align: center;">Mô tả</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody >
                            @foreach($dep as $item)
                                <tr>
                                    <td class="text-center"><span>{{$item->id}}</span></td>
                                    <td class="text-center"><span>{{$item->name}}</span></td>
                                    <td class="text-center"><span>{{$item->description}}</span></td>
                                    <td style="width: 15%;">
                                        <a class="btn btn-sm btn-warning" href='javascript:window.location.href ="admin/department/edit/{{$item->id}}"' title="">Sửa</a>
                                        <a onclick="return confirm('Bạn có chắc muốn xoá phòng ban này')" class="btn btn-sm btn-danger" href="javascript:window.location.href ='admin/department/destroy/{{$item->id}}'" title="">Xoá</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
