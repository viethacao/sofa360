@extends('admin.widget.index')
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row" style="margin-bottom: 30px;">
            <h4>Tìm thấy {{count($categories)}}</h4>
            @foreach($categories as $r)
                <div class="col-md-4 col-sm-4">
                    <div class="card" style="width: 18rem;">
                        <a href="admin/categories/detail/{{$r->id}}">
                       
                        <div class="card-body">
                            <h4 class="card-title" style="font-size: 16px;">{{$r->name}}</h4>
                            <a href="admin/categories/detail/{{$r->id}}" style="margin-bottom: 10px;" class="btn btn-primary btn-sm">Xem danh mục</a>
                            <a href="admin/categories/destroy/{{$r->id}}" style="margin-bottom: 10px;" class="btn btn-danger btn-sm">Xóa danh mục</a>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</div>
</div> 
@endsection
