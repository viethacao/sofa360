@extends('admin.widget.index')
@section('content')    	
<div class="be-content">
	<div class="main-content container-fluid">
		<div class="row">
			<form class="form-horizontal" role="form" action="admin/employees/update/{{$emp->id}}" method="post" enctype="multipart/form-data">
				<div class="form-group text-center">
					@if(session('success'))
                		<div class="alert-success text-center" style="width: 100%;display: block;padding: 10px;margin: 20px 0px;">
                    	{{session('success')}}
                		</div>
            		@endif
                    <label for=""  style="width: 113px;">Nhập tên:</label> 
                    <input name="name" type="text" value="{{$emp->name}}" required placeholder="nhập tên nhân viên" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>
					
					<label style="width: 113px;">Nhập phòng ban:</label>
					<select name="dep" id="" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;">
					@foreach ($dep as $item)
						@if ($item->id === $emp->department_id)
							<option style="display:none;" selected value="{{$item->id}}">{{$item->name}}</option>  
						@endif
						<option value="{{$item->id}}">{{$item->name}}</option>
					@endforeach
					</select> <br> <br>
					
					<label style="width: 113px;">Nhập ngày sinh:</label> 
					<input name="date" type="date" value="{{substr($emp->date_of_birth,-22, -9)}}" required placeholder="nhập ngày sinh" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>
					
					<label style="width: 113px;">Nhập địa chỉ:</label> 
					<input name="address" type="text" value="{{$emp->address}}" required placeholder="nhập địa chỉ " style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>
					
					<label style="width: 113px;">Nhập giới tính:</label> 
					<select name="gender" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;">
						<option selected style="display:none">{{$emp->gender}}</option>
						<option>Nam</option>
						<option>Nữ</option>
					</select> <br><br>

					<label style="width: 113px;">Nhập lương:</label> 
					<input name="salary" type="number" value="{{$emp->salary}}" required placeholder="nhập lương" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>
					
					<label style="width: 113px;">Nhập vị trí làm việc:</label> 
					<input name="position" type="text" value="{{$emp->position}}" required placeholder="nhập vị trí" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>
					
					<label style="width: 113px;">Nhập ngày bắt đầu làm việc:</label> 
                    <input name="date_start" type="date" value="{{substr($emp->date_start,-22, -9)}}" required style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;"> <br> <br>
					
					<label style="width: 113px;">Nhập trạng thái</label>
					<select name="status" style="width: 40%;padding: 10px; border: 1px solid #ccc;border-radius: 10px;outline: none;">
						@if (($emp->status) == 1)
							<option selected style="display:none" value="1">Đang làm việc</option>
						@else 
							<option selected style="display:none" value="0">Không còn làm việc</option>						
						@endif
						<option value="1">Đang làm việc</option>
						<option value="0">Không còn làm việc</option>
					</select>
					
				</div>
				<button class="btn btn-danger" style="display: block; margin: auto;">Cập nhập</button>
                <a href="admin/employees" style=" display: table; margin: 10px auto;" class="btn btn-success">Quay lại</a>

            </form>
			
		</div>
	</div>
</div>
@endsection