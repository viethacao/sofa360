<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Route::get('admin/login','useradController@getlogin');
Route::post('admin/login','useradController@postlogin')->name('admin/login');

Route::post('admin/postlogout','UseradController@postlogout');

Route::get('create','useradController@create');
Route::post('create', 'useradController@store')->name('create');


Route::group(['prefix'=>'admin','middleware'=>'adminLogin'],function(){
	// Route::get('/','useradController@getadmin');
	Route::get('/',function(){
		$totalemployee = DB::table('employees')->count();
		$totalproduct = DB::table('products')->count();
		$totalcustomer = DB::table('customers')->count();
		$totalcategory = DB::table('categories')->count();
		return view('admin.home.home',['totalemployee'=>$totalemployee,'totalproduct'=>$totalproduct,'totalcustomer'=>$totalcustomer,'totalcategory'=>$totalcategory]);
	});

	Route::group(['prefix' => 'user'], function()
	{
		Route::get('/','useradController@index');

		Route::get('info/{id}','useradController@info');

		Route::get('on/{id}','useradController@on');
		Route::get('off/{id}','useradController@off');

		Route::get('edit/{id}','useradController@edit');
		Route::post('update/{id}','useradController@update');
		Route::post('updateimage/{id}','useradController@updateimage');
		Route::post('repass/{id}','useradController@repass');

		Route::get('delete/{id}','useradController@delete');
	});

	Route::group(['prefix'=>'employees'], function () 
	{
		Route::get('/','EmployeesController@index');

		Route::get('create','EmployeesController@create');
		Route::post('store','EmployeesController@store');

		Route::get('destroy/{id}','EmployeesController@destroy');

		Route::get('on/{id}','EmployeesController@on');
		Route::get('off/{id}','EmployeesController@off');

		Route::get('edit/{id}','EmployeesController@edit');
		Route::post('update/{id}','EmployeesController@update');
	});



	Route::group(['prefix'=>'department'], function () 
	{
		Route::get('/','DepartmentsController@index');

		Route::get('create','DepartmentsController@create');
		Route::post('store','DepartmentsController@store');

		Route::get('edit/{id}','DepartmentsController@edit');
		Route::post('update/{id}', [
			'as' => 'update-dep',
			'uses' => 'DepartmentsController@updateDep'
		]);

		Route::get('destroy/{id}','DepartmentsController@destroy');

		
	});

	Route::group(['prefix'=>'bill'], function () 
	{
		Route::get('/','BillController@index');

		Route::get('add-pro','BillController@addPro');
		Route::post('store','BillController@store');
		Route::get('add-cart/{id}', [
			'as' => 'add-cart',
			'uses' => 'BillController@addCart'
		]);

		Route::get('create-bill', [
			'as' => 'create-bill',
			'uses' => 'BillController@createBill'
		]);

		Route::get('update-pro', [
			'as' => 'update-pro',
			'uses' => 'BillController@updatePro'
		]);

		Route::get('delete-pro/{id}', [
			'as' => 'delete-pro',
			'uses' => 'BillController@deletePro'
		]);

		Route::get('delete-bill/{id}', [
			'as' => 'delete-bill',
			'uses' => 'BillController@deleteBill'
		]);

		Route::get('edit-bill/{id}', [
			'as' => 'edit-bill',
			'uses' => 'BillController@editBill'
		]);
		Route::post('update-bill/{id}', [
			'as' => 'update-bill',
			'uses' => 'BillController@updateBill'
		]);

		Route::get('details-bill/{id}', [
			'as' => 'details-bill',
			'uses' => 'BillController@detailsBill'
		]);
	});

	Route::group(['prefix'=>'products'], function () 
	{
		Route::get('/','productsController@index');

		Route::get('createsv','productsController@create');
		Route::post('createsv','productsController@store');

		Route::get('detail/{id}','productsController@detail');

		Route::get('destroy/{id}','productsController@destroy');

		Route::get('on/{id}','productsController@on');
		Route::get('off/{id}','productsController@off');

		Route::get('getupdate/{id}','productsController@getupdate');
		Route::post('postupdate/{id}','productsController@postupdate');

		Route::get('editimage/{id}','productsController@getedit');
		Route::post('posteditimage/{id}','productsController@postedit');

		Route::post('editroom/{id}','productsController@editroom');
	});

	Route::group(['prefix'=>'categories'], function () 
	{
		Route::get('/','categoriesController@index');

		Route::get('createcategory','categoriesController@createcategory');
		Route::post('store','categoriesController@store');

		Route::get('on/{id}','categoriesController@on');
		Route::get('off/{id}','categoriesController@off');

		Route::get('detail/{id}','categoriesController@detail');

		Route::get('getupdate/{id}','categoriesController@getupdate');
		Route::post('postupdate/{id}','categoriesController@update');

		Route::get('search','categoriesController@search');

		Route::post('update/{id}','categoriesController@update');

		Route::get('destroy/{id}','categoriesController@destroy');
	});

	Route::group(['prefix'=>'customer'], function(){
		Route::get('','CustomersController@index');

		Route::get('edit/{id}','CustomersController@edit');
		Route::post('update/{id}','CustomersController@update');
	});


});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
