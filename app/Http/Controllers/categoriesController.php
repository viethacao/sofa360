<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreRoomRequest;

class categoriesController extends Controller
{
     //
    public function index(){
        $categories = Category::all();
        return view('admin.categories.index',['categories'=>$categories]);
    }

    public function createcategory(){
    	return view('admin.categories.create');
    }

    public function store(StoreRoomRequest $request)
    {
        $categories = Category::create($request->all());

        return redirect('admin/categories')->with('thongbao','Thêm danh mục thành công hãy kiểm tra lại');
    }

    public function on($id)
    {
        $room = Room::find($id);  
        if ($room)
        {
            $room->status = 1;
            $room->save();
            return redirect('admin/room');
        }
        else {
            return redirect('admin/room');
        } 
    }

    public function off($id)
    {
        $room = Room::find($id);
        $students = $room->students;
        if ($room) 
        {
            $room->status = 0;
            $room->save();
            return redirect('admin/room');
        }
        else 
            return redirect('admin/room');
    }

    public function detail(Request $request,$id)
    {
        $categories = Category::find($id);
        $products = Products::all();
        $categories_id = $categories->id;
        return view('admin.categories.infobackend',compact('categories','products','categories_id'));
    }
     public function getupdate($id)
    {
        $categories = Category::find($id);
        return view('admin.categories.update',['categories' => $categories]);
    }
    public function update(Request $request,$id)
    {
        $categories = Category::find($id);
        $categories->name = $request->name;
        $categories->description = $request->description;
       
        $categories->save();
        return redirect('admin/categories/getupdate/'.$id)->with('thongbao','Chỉnh sửa thông tin thành công');
    }

    public function destroy($id)
    {
        $categories = Category::find($id);
        $categories->delete();
        return redirect('admin/categories');
    }

    public function search(Request $request)
    {
        $key = $request->key;
        $categories = Category::where('name','like',"%{$key}%")->get();
        // dd($rooms);
        return view('admin/categories/search',compact('categories'));
    }

    /**
     * @param $file
     * @return mixed
     */
    public function upload($file, $path)
    {
        $name = sha1(date('YmdHis') . str_random(30)) . str_random(2) . '.' . $file->getClientOriginalExtension();

        $file->move($path, $name);

        return $path . $name;
    }
}
