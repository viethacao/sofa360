@extends('admin.widget.index')
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="page-title">
                <div class="title_left">
                    <a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a><span>/</span><a href="admin/customer" style="font-size: 20px;">Khách hàng</a>
                    <h3>Danh sách khách hàng</h3>
            </div>
        </div>
            <div class="text-center">
                @if(session('success'))
                <div class="alert-success text-center" style="width: 100%;display: block;padding: 10px;margin: 20px 0px;">
                    {{session('success')}}
                </div>
            @endif 
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                        <table class="table table-hover table-bordered table12" style="margin-top: 15px;">
                        <thead>
                            <tr>
                                <th style="width: 15%; text-align: center;">Tên khách hàng</th>
                                <th style="width: 10%; text-align: center;">Giới tính</th>
                                <th style="width: 25%; text-align: center;">Email</th>
                                <th style="width: 20%; text-align: center;">Địa chỉ</th>
                                <th style="width: 10%; text-align: center;">Số điện thoại</th>
                                <th style="width: 10%; text-align: center;">Cập nhập</th>
                            </tr>
                        </thead>
                        <tbody >
                            @foreach($cus as $item)
                                <tr>
                                    <td class="text-center"><span>{{$item->name}}</span></td>
                                    <td class="text-center"><span>@if($item->gender == 0)Nam @else Nữ @endif</span></td>
                                    <td class="text-center"><span>{{$item->email}}</span></td>
                                    <td><span>{{$item->address}}</span></td>
                                    <td class="text-center"><span>{{$item->phone}}</span></td>
                                    <td><a class="btn btn-sm btn-warning" href='javascript:window.location.href ="admin/customer/edit/{{$item->id}}"' title="">Sửa</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
