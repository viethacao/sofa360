@extends('admin.widget.index')
@section('content')
<div class="be-content">
	<div class="main-content container-fluid">
		<div class="row">
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="x_panel">
						<div class="x_content">
							<a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a>
								<h2>Thống kê</h2>
							<div class="main-content container-fluid">
								<div class="row">
									<div class="col-xs-12 col-md-6 col-lg-3">
										<div class="widget widget-tile" style="background: #f4c842;">
											<div class="desc">Số nhân viên</div>
											<div class="value">
												<span data-toggle="counter" data-end="113" class="number">{{$totalemployee}}</span><i class="iconhome zmdi zmdi-accounts zmdi-hc-lg" style="margin-left: 5px;"></i>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-6 col-lg-3">
										<div class="widget widget-tile" style="background: #f29ff2; ">
											<div class="desc">Số sản phẩm</div>
											<div class="value">
												<span data-toggle="counter" data-end="113" class="number">{{$totalproduct}}</span><i class="iconhome zmdi zmdi-accounts zmdi-hc-lg" style="margin-left: 5px;"></i>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-6 col-lg-3">
										<div class="widget widget-tile" style="background: #8bd9f4;">
											<div class="desc">Số khách hàng</div></i>
											<div class="value">
												<span data-toggle="counter" data-end="113" class="number">{{$totalcustomer}}</span><i class="iconhome zmdi zmdi-accounts zmdi-hc-lg" style="margin-left: 5px;"></i>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-6 col-lg-3">
										<div class="widget widget-tile" style="background: #d198f9;">
											<div class="desc">Số loại sản phẩm</div>
											<div class="value">
												<span data-toggle="counter" data-end="113" class="number">{{$totalcategory}}</span><i class="fab fa-product-hunt" style="margin-left: 5px;"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
							<h2 style="text-align: center;">Lịch sử phát triển của SOFA 360</h2>
							<h3 style="text-align: inherit;">Không chỉ đơn thuần đem tới những sản phẩm nội thất chất lượng và tỉ mỉ tới từng chi tiết, SOFA360 còn thổi một làn gió mới vào những thiết kế ấy, mang lại cảm hứng từ sự chuẩn mực và tinh tế khó cưỡng.</h3>
							<h4>Nếu đã từng ghé thăm showroom của chúng tôi, chắc hẳn bạn sẽ cảm nhận được tính thẩm mỹ được thấm đẫm trong cuộc sống. Đặc biệt, SOFA360 luôn mang một sắc thái riêng không trộn lẫn, để lại nhiều ấn tượng, thường được coi là chuẩn mực của sofa trong nước. Nhắc tới SOFA360, có lẽ không thể không nhắc tới sofa da nổi tiếng Hà Nội bởi chất lượng cao cấp. Trong số đó, sofa da – một thương hiệu nội thất lâu đời, hoạt động từ năm 1997 là một cái tên nổi bật. Từ khi ra đời tới nay, thương hiệu sofa da của SOFA360 đã phát triển nhanh chóng từ một xưởng nhỏ được người dân trong tỉnh lẻ ưa chuộng, trở thành tên tuổi hàng đầu Việt Nam trong lĩnh vực sofa.</h4>
							<h3>Khởi đầu từ câu chuyện chinh phục những khách hàng khó tính</h3>
							<h4>Đã từ lâu, Hà Nội nổi tiếng là quốc gia có gu ăn mặc lịch lãm và cá tính nhất cả nước, người dân nơi đây coi thời trang và thẩm mỹ là một phần không thể thiếu trong đời sống văn hóa. Trong quan niệm của người Hà Nội, phong cách là thứ không thể mua được, mà phải qua quá trình trau dồi kiến thức, tiếp thu cái đẹp. Chính thế, hầu hết các hãng thời trang, các công ty thiết kế, sản xuất đồ tiêu dùng, nội thất lâu đời đều thấm nhuần văn hóa đó. SOFA360 cũng không phải ngoại lệ.</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
@endsection