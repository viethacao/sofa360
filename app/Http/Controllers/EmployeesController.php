<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Employees;
use App\Department;
use App\Http\Requests\StoreStudentRequest;

class EmployeesController extends Controller
{

    public function index()
    {
        $emp = Employees::all();
       
        return view('admin.employees.index', compact('emp'));
    }

    public function create()
    {
        $dep = Department::all();
    	return view('admin.employees.add', compact('dep'));
    }

    public function store(Request $req)
    {   
        $emp = new Employees;
        $emp->name = $req->name;
        $emp->date_of_birth = $req->date;
        $emp->address = $req->address;
        $emp->salary = $req->salary;
        $emp->gender = $req->gender;
        $emp->position = $req->position;
        $emp->date_start = $req->date_start;
        $emp->department_id = $req->dep;
        $emp->status = $req->status;
        $emp->save();
        return redirect('admin/employees')->with('success','Thêm mới thành công');
    }

    
    public function on($id)
    {
        $emp = Employees::find($id);
        if ($emp)
        {
            $emp->status = 1;
            $emp->save();
            return redirect('admin/employees');
        }
        else
        {
            return redirect('admin/employees');
        }    
    }

    public function off($id)
    {
        $emp = Employees::find($id);
        if ($emp)
        {
            $emp->status = 0;
            $emp->save();
            return redirect('admin/employees');
        }
        else
        {
            return redirect('admin/employees');
        }    
    }

    public function destroy($id)
    {
        $emp = employees::find($id);
        $emp->delete();
        return redirect('admin/employees');
    }

    public function edit($id)
    {
        $emp = employees::find($id);
        $dep = Department::all();
        return view('admin.employees.edit',compact('emp', 'dep'));
    }  

    public function update(Request $req, $id)
    {
        $emp = employees::find($id);
        $emp->name = $req->name;
        $emp->date_of_birth = $req->date;
        $emp->address = $req->address;
        $emp->salary = $req->salary;
        $emp->gender = $req->gender;
        $emp->position = $req->position;
        $emp->date_start = $req->date_start;
        $emp->department_id = $req->dep;
        $emp->status = $req->status;
        $emp->save();
        return redirect('admin/employees')->with('success','Cập nhật thông tin thành công');
    }
}
