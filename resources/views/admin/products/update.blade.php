@extends('admin.widget.index')
@section('content')
<div class="be-content">
	<div class="main-content container-fluid">
		<div class="row">
			<div class="page-title">
				<div class="title_left">
					<h3>Chỉnh sửa sản phẩm</h3>
				</div>
			</div>
			<div class="clearfix"></div>
			@if(count($errors)>0)
			<div class="alert alert-danger">
				@foreach($errors->all() as $er)
				{{$er}}<br>
				@endforeach
			</div>
			@endif

			@if(session('thongbao'))
			<div class="alert alert-success">
				{{session('thongbao')}}
			</div>
			@endif
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<form class="form-horizontal" role="form" action="admin/products/postupdate/{{$products->id}}" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{csrf_token()}}"/>
						<div class="form-group">
							<label class="col-sm-2">Tên sản phẩm</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="name" value="{{$products->name}}">
							</div>
						</div>	
						<div class="form-group">
							<label class="col-sm-2">Mô tả</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="description" value="{{$products->description}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2">Giá</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="price" value="{{$products->price}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2">Kích thước</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="size" value="{{$products->size}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2">Màu</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="color" value="{{$products->color}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2">Số lượng</label>
							<div class="col-sm-6">
								<input type="number" class="form-control" name="quantity" value="{{$products->quantity}}">
							</div>
						</div>
						<div class="form-group text-center">
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
							</div>
							<div class="col-sm-3 col-sm-3">
								<button type="submit" class="btn btn-success">Cập nhập</button>
								<button type="reset" class="btn btn-default">Làm lại</button>
							</div>
						</div>
					</form>
					<div class="form-group text-center col-sm-9 col-sm-9">
						<a href="{{'admin/products'}}"><button class="btn btn-rounded btn-space btn-warning">Quay lại</button></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection