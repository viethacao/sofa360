<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
class DepartmentsController extends Controller
{
    public function index()
    {
        $dep = Department::all();
        return view('admin.department.index', compact('dep'));
    }

    public function create()
    {
    	return view('admin.department.add');
    }

    public function store(Request $req)
    {
        $dep = new Department;
        $dep->name = $req->name;
        $dep->description = $req->desc;
        $dep->save();
        return redirect('admin/department')->with('success','Thêm mới thành công');
    }

    public function edit($id)
    {
        $dep = Department::find($id);
        return view('admin.department.edit',compact('dep'));
    }

    public function updateDep(Request $req, $id)
    {
        $dep = Department::find($id);
        $dep->name = $req->name;
        $dep->description = $req->desc;
        $dep->save();
        return redirect('admin/department')->with('success','Cập nhật thành công');
    }

    public function destroy($id)
    {
        $dep = Department::find($id);
        $dep->delete();
        return redirect()->back();
    }
}
