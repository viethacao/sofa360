	@extends('admin.widget.index')
	@section('content')     
	<div class="be-content">
		<div class="main-content container-fluid">
			<div class="user-profile col-md-3" style="float: left;">
				<img src="{{asset('admin_asset')}}/img/imgroom/ảnh 1.jpg" alt="" style="width: 100%">
			</div>
			<a href="admin/categories" style="display: table; margin: auto;" class="btn btn-primary btn-lg">Quay lại</a>
			<div class="col-md-8" style="float: right;">
				<div><h2>Loại sản phẩm {{$categories->name}}</h2></div>
				<label for="cate" style="font-size: 20px;">Mô tả: {{$categories->description}}.</label>
				<label style="font-size: 20px;" for="pro"><b>Các sản phẩm:</b></label>
				<table class="table">
					<thead>
						<tr>
							<th>Tên sản phẩm</th>
							<th>Description</th>
							<th>Price</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
							@foreach($products as $products)
								@if($products->categories_id == $categories_id)
									<tr>
										<td>{{$products->name}}</td>
										<td>{{$products->description}}</td>
										<td>{{$products->price}}</td>
										<td><a href="admin/products/detail/{{$products->id}}">Chi tiết</a></td> 	
									</tr>
								@endif
							@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection