@extends('admin.widget.index')
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="admin" class="zmdi zmdi-home" style="font-size: 24px;">Trang chủ</a><span>/</span><a href="admin/room" style="font-size: 20px;">Loại sản phẩm</a>
                <div class="alert alert-heading" role="alert" align="center">
                    <strong><h1>Danh sách các loại sản phẩm</h1></strong>
                </div>
            </div>
        </div>
        @if(session('thongbao'))
            <div class="alert alert-success">
            {{session('thongbao')}}
            </div>
        @endif
          <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                        <table class="table table-hover table-bordered table12" style="margin-top: 15px;">
                            <thead>
                                <tr>
                                    <th style="width: 5%; text-align: center;">ID</th>
                                    <th style="width: 15%; text-align: center;">Tên danh mục</th>
                                    <th style="width: 20%; text-align: center;">Mô tả</th>
                                    <th style="width: 20%; text-align: center;">Thao tác</th>
                                </tr>
                            </thead>
                            <tbody >
                                @foreach($categories as $pr)
                                    <tr>
                                        <td style="width: 5%;" class="text-center"><span>{{$pr->id}}</span></td>
                                        <td style="width: 10%;" class="text-center"><span>{{$pr->name}}</span></td>
                                        <td style="width: 25%;" class="text-center"><span >{{$pr->description}}</span></td>
                                        <td style="width: 10%">
                                            <a class="btn btn-sm btn-primary" href="javascript:window.location.href ='admin/categories/detail/{{$pr->id}}'" title="">Xem</a>
                                            <a onclick="return confirm('Bạn có chắc chắn xoá không?')" class="btn btn-sm btn-danger" href="javascript:window.location.href ='admin/categories/destroy/{{$pr->id}}'" title="">Xoá</a>
                                            <a class="btn btn-sm btn-primary" href="javascript:window.location.href ='admin/categories/getupdate/{{$pr->id}}'" title="">Sửa</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>
</div> 
@endsection
